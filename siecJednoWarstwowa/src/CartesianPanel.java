import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class CartesianPanel extends JPanel{
	Perceptron p1;
	Perceptron p2;
	Perceptron p3;
	double [][] inputs ={
		  	{ -25, -20 }, { -42, -15 }, { -15, - 17 },
            { -12, -12 }, { -9, -25 }, { -10, - 12 },
            { -21, -30 }, { -4, -30 }, { -11,  -14 },
            { 12, 13 }, { 16, 18 }, { 18, 14 },
            { 12, 19 }, { 42, 50 }, { 17, 11 },
            { 22, 13 }, { 32, 26 }, { 33,  13 },
            { -50, 50 }, { -60, 48 }, { -66, 65 },
            { -55, 60 }, { -42, 84 }, { -70, 48 },
            { -49, 70 }, { -82, 46 }, { -73,  40 }
   };
   int[] outputs1= 
 	    {1, 1, 1, 1, 1, 1, 1, 1, 1,
 	    -1,-1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,-1};
   int[] outputs2= 
 	   {-1,-1,-1,-1,-1,-1,-1,-1,-1,
 	     1, 1, 1, 1, 1, 1, 1, 1, 1,
        -1,-1,-1,-1,-1,-1,-1,-1,-1};
   int[] outputs3= 
 	   {-1,-1,-1,-1,-1,-1,-1,-1,-1,
 		-1,-1,-1,-1,-1,-1,-1,-1,-1,
         1, 1, 1, 1, 1, 1, 1, 1, 1};
	public CartesianPanel() {
		
		this.setSize(500, 500);
		this.setVisible(true);
		p1 = new Perceptron(inputs,outputs1);
		p2 = new Perceptron(inputs,outputs2);
		p3 = new Perceptron(inputs,outputs3);
		
	} 
	private void drawColor(int i, int j, Graphics2D g2d, Color c){

		  g2d.setPaint(c);
		  g2d.fillRect(i, j, 5, 5);
		  
		  g2d.setPaint(Color.black);
		  g2d.drawRect(i, j, 5, 5);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		  Graphics2D g2d = (Graphics2D) g;
		  
	     
	      
	      for(int i = -250; i < 250; i+=5){
	    	  for(int j = -250;j < 250; j+=5){
	    		  if(p1.returnOutput(j, i) == 1)
	    			  drawColor(-j+250,i+250,g2d,Color.red);
	    		  else if(p2.returnOutput(j, i) == 1)
	    			  drawColor(-j+250,i+250,g2d,Color.blue);
	    		  else if(p3.returnOutput(j, i) == 1)
	    			  drawColor(-j+250,i+250,g2d,Color.magenta);
	    		  
	    	  }
	      }
	      
	      for(int a = 0; a < inputs.length;a++){
	    	  if(outputs1[a] == 1)
    			  drawColor((int)-inputs[a][0]+250,(int)inputs[a][1]+250,g2d,Color.green);
    		  else if(outputs2[a] == 1)
    			  drawColor((int)-inputs[a][0]+250,(int)inputs[a][1]+250,g2d,Color.yellow);
    		  else if(outputs3[a] == 1)
    			  drawColor((int)-inputs[a][0]+250,(int)inputs[a][1]+250,g2d,Color.pink);
	      }
	      drawColor(250,250,g2d,Color.CYAN);
	}


	
}
