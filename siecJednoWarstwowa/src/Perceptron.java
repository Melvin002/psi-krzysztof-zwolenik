import java.util.Random;
import java.util.Scanner;

public class Perceptron {

	double weights [];
	double [][] inputs;
	double bias;
	int outputs [];

	public Perceptron(double [][] inputs, int[] outputs) {
		Scanner in = new Scanner(System.in);
		// TODO Auto-generated method stub
		 this.inputs = inputs;
		 this.outputs = outputs;
		 int inputsCount = inputs.length;
		 
		 Random r = new Random();
		 weights = new double [] {r.nextDouble(), r.nextDouble() };
		 bias = r.nextDouble();
		 double learningRate = 0.1;
		 
		 int iteration = 0;
         double globalError;
         
         do{
        	 globalError = 0;
        	 for(int i = 0; i < inputsCount; i++){
        		 
        		// Calculate output.
                 int output = Output(weights, inputs[i][0], inputs[i][1]);
                 
                // Calculate error.
                 double localError = outputs[i] - output;
                 
                 if (localError != 0)
                 {
                     // Update weights.
                     for (int j = 0; j < 2; j++)
                     {
                         weights[j] += learningRate * localError * inputs[i][j];
                     }
                     bias+=localError*learningRate;
                 }
                 // Convert error to absolute value.
                 globalError += Math.abs(localError);
                 
                 //System.out.println("Iteration " + (iteration%18+1) + " \t Error " + globalError);
                 iteration++;
        	 }
        	 
         }while(globalError != 0);

         
	}
	private int Output(double weights[], double x, double y){
		double sum = bias;
		sum += x * weights[0] + y * weights[1];
		return (sum >= 0) ? 1 : -1;
	}
	public int returnOutput(int x, int y){
		return Output(weights,x,y);
	}
	public double[] getWeights() {
		return weights;
	}

}
