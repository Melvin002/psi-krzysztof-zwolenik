from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy
import os
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import cm
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.constraints import maxnorm
from keras.utils import np_utils
from sklearn.preprocessing import StandardScaler

# load json and create model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

# load my data
my_data = pd.read_csv('liczby.csv').values.astype('float32')
my_data = StandardScaler().fit(my_data).transform(my_data)
my_data = my_data.reshape(-1,28,28,1)

# predictive model
test_submission = loaded_model.predict_classes(my_data, verbose=2)

for i in range(9):
    plt.subplot(330+i+1)
    plt.imshow(my_data.reshape(-1,1,28,28)[i][0],cmap=cm.binary)
plt.show()
print(test_submission[:9])