/*
 * Kohonen.java
 *
 * Created on 15 de Setembro de 2002, 07:49
 */

/*
 * @author  Marcelo Ivan Martin
 * mivan@via.com.br
 * Professora: Rejane Frozza
 * Mail: frozza@inf.ufrgs.br
 */

import java.util.*;
import java.math.*;
import javax.swing.*;
import java.awt.*;

public class Kohonen extends javax.swing.JFrame {

    //1d x
    //2d y
    //3d 0 = x
    //3d 1 = y
    
    //matriz de pesos
    //a terceira dimensao assume 0 e 1, onde se referem a x e y respectivamente
    public double pesos[][][];
    
    public final int DIMX = 10; //dimensao horizontal
    public final int DIMY = 10; //dimensao vertical
    public final int DIMZ = 2; //pesos por neuronio
    public double alpha = 0.9; //coeficiente de aprendizado
    public int figuras[][][]; //matriz das figuras
    public int DESENHO_MULT = 0; //multiplicador para desenhar o quadro
    public int grauVizinhanca = 3;
    
    /** Creates new form Kohonen */
    public Kohonen() {

        //no inicio inicializa os componentes graficos
        initComponents();
        //calcula a fracao de cada ponto no painel de desenho
        DESENHO_MULT = jPanel1.getWidth()/DIMX;
        //carrega as entradas das figuras
        inicializaFigutas();
        //sorteio dos pesos
        inicializaPesos();
    }
    
    public void inicializaFigutas(){
        
        //inicializacao das figuras da entrada
        //as opcoes de figuras estao na combo jComboBox1
        
        //1d numero do ponto
        //2d posicao x ou y
        //3d 0 = quadrado
        figuras = new int[18][2][5];
        figuras[0][0][0] = 0;
        figuras[1][0][0] = 2;
        figuras[2][0][0] = 4;
        figuras[3][0][0] = 6;
        figuras[4][0][0] = 8;
        figuras[5][0][0] = 9;
        figuras[6][0][0] = 0;
        figuras[7][0][0] = 9;
        figuras[8][0][0] = 0;
        figuras[9][0][0] = 9;
        figuras[10][0][0] = 0;
        figuras[11][0][0] = 9;
        figuras[12][0][0] = 0;
        figuras[13][0][0] = 1;
        figuras[14][0][0] = 3;
        figuras[15][0][0] = 5;
        figuras[16][0][0] = 7;
        figuras[17][0][0] = 9;
        figuras[0][1][0] = 0;
        figuras[1][1][0] = 0;
        figuras[2][1][0] = 0;
        figuras[3][1][0] = 0;
        figuras[4][1][0] = 0;
        figuras[5][1][0] = 1;
        figuras[6][1][0] = 2; 
        figuras[7][1][0] = 3; 
        figuras[8][1][0] = 4; 
        figuras[9][1][0] = 5; 
        figuras[10][1][0] = 6; 
        figuras[11][1][0] = 7;
        figuras[12][1][0] = 8;
        figuras[13][1][0] = 9;
        figuras[14][1][0] = 9;
        figuras[15][1][0] = 9;
        figuras[16][1][0] = 9;
        figuras[17][1][0] = 9;

        //3d = 1 = circulo
        figuras[0][0][1] = 3;
        figuras[1][0][1] = 4;
        figuras[2][0][1] = 5;
        figuras[3][0][1] = 2;
        figuras[4][0][1] = 7;
        figuras[5][0][1] = 1;
        figuras[6][0][1] = 8;
        figuras[7][0][1] = 0;
        figuras[8][0][1] = 9;
        figuras[9][0][1] = 0;
        figuras[10][0][1] = 9;
        figuras[11][0][1] = 1;
        figuras[12][0][1] = 8;
        figuras[13][0][1] = 2;
        figuras[14][0][1] = 7;
        figuras[15][0][1] = 4;
        figuras[16][0][1] = 5;
        figuras[17][0][1] = 6;
        figuras[0][1][1] = 0;
        figuras[1][1][1] = 0;
        figuras[2][1][1] = 0;
        figuras[3][1][1] = 1;
        figuras[4][1][1] = 1;
        figuras[5][1][1] = 2;
        figuras[6][1][1] = 2; 
        figuras[7][1][1] = 4; 
        figuras[8][1][1] = 4; 
        figuras[9][1][1] = 5; 
        figuras[10][1][1] = 5; 
        figuras[11][1][1] = 7;
        figuras[12][1][1] = 7;
        figuras[13][1][1] = 8;
        figuras[14][1][1] = 8;
        figuras[15][1][1] = 9;
        figuras[16][1][1] = 9;
        figuras[17][1][1] = 9;

        //3d = 2 = triangulo
        figuras[0][0][2] = 4;
        figuras[1][0][2] = 5;
        figuras[2][0][2] = 3;
        figuras[3][0][2] = 6;
        figuras[4][0][2] = 2;
        figuras[5][0][2] = 2;
        figuras[6][0][2] = 7;
        figuras[7][0][2] = 1;
        figuras[8][0][2] = 1;
        figuras[9][0][2] = 8;
        figuras[10][0][2] = 0;
        figuras[11][0][2] = 8;
        figuras[12][0][2] = 0;
        figuras[13][0][2] = 2;
        figuras[14][0][2] = 4;
        figuras[15][0][2] = 6;
        figuras[16][0][2] = 8;
        figuras[17][0][2] = 9;
        figuras[0][1][2] = 0;
        figuras[1][1][2] = 1;
        figuras[2][1][2] = 2;
        figuras[3][1][2] = 3;
        figuras[4][1][2] = 4;
        figuras[5][1][2] = 5;
        figuras[6][1][2] = 5; 
        figuras[7][1][2] = 6; 
        figuras[8][1][2] = 7; 
        figuras[9][1][2] = 7; 
        figuras[10][1][2] = 8; 
        figuras[11][1][2] = 8;
        figuras[12][1][2] = 9;
        figuras[13][1][2] = 9;
        figuras[14][1][2] = 9;
        figuras[15][1][2] = 9;
        figuras[16][1][2] = 9;
        figuras[17][1][2] = 9;

        //3d = 3 = trapezio
        figuras[0][0][3] = 0;
        figuras[1][0][3] = 1;
        figuras[2][0][3] = 3;
        figuras[3][0][3] = 4;
        figuras[4][0][3] = 5;
        figuras[5][0][3] = 7;
        figuras[6][0][3] = 8;
        figuras[7][0][3] = 9;
        figuras[8][0][3] = 0;
        figuras[9][0][3] = 9;
        figuras[10][0][3] = 1;
        figuras[11][0][3] = 8;
        figuras[12][0][3] = 2;
        figuras[13][0][3] = 7;
        figuras[14][0][3] = 3;
        figuras[15][0][3] = 4;
        figuras[16][0][3] = 5;
        figuras[17][0][3] = 6;
        figuras[0][1][3] = 8;
        figuras[1][1][3] = 8;
        figuras[2][1][3] = 8;
        figuras[3][1][3] = 8;
        figuras[4][1][3] = 8;
        figuras[5][1][3] = 8;
        figuras[6][1][3] = 8; 
        figuras[7][1][3] = 8; 
        figuras[8][1][3] = 7; 
        figuras[9][1][3] = 7; 
        figuras[10][1][3] = 6; 
        figuras[11][1][3] = 6;
        figuras[12][1][3] = 5;
        figuras[13][1][3] = 5;
        figuras[14][1][3] = 4;
        figuras[15][1][3] = 4;
        figuras[16][1][3] = 4;
        figuras[17][1][3] = 4;
    
        //3d = 4 = hexagono
        figuras[0][0][4] = 3;
        figuras[1][0][4] = 4;
        figuras[2][0][4] = 5;
        figuras[3][0][4] = 6;
        figuras[4][0][4] = 2;
        figuras[5][0][4] = 7;
        figuras[6][0][4] = 1;
        figuras[7][0][4] = 8;
        figuras[8][0][4] = 0;
        figuras[9][0][4] = 9;
        figuras[10][0][4] = 1;
        figuras[11][0][4] = 8;
        figuras[12][0][4] = 2;
        figuras[13][0][4] = 7;
        figuras[14][0][4] = 3;
        figuras[15][0][4] = 4;
        figuras[16][0][4] = 5;
        figuras[17][0][4] = 6;
        figuras[0][1][4] = 3;
        figuras[1][1][4] = 3;
        figuras[2][1][4] = 3;
        figuras[3][1][4] = 3;
        figuras[4][1][4] = 4;
        figuras[5][1][4] = 4;
        figuras[6][1][4] = 5; 
        figuras[7][1][4] = 5; 
        figuras[8][1][4] = 6; 
        figuras[9][1][4] = 6; 
        figuras[10][1][4] = 7; 
        figuras[11][1][4] = 7;
        figuras[12][1][4] = 8;
        figuras[13][1][4] = 8;
        figuras[14][1][4] = 9;
        figuras[15][1][4] = 9;
        figuras[16][1][4] = 9;
        figuras[17][1][4] = 9;    
    }

    private void desenha(){

        //Desenha a tela fazendo o tracado entre os pontos
        
        //contadores para varrer as duas dimensoes da matriz
        int contX;
        int contY;
        
        //pega o objeto grafico
        Graphics g = jPanel1.getGraphics();
        
        g.setColor(Color.black);
        
        //limpa a area
        g.clearRect(0,0,jPanel1.getWidth(),jPanel1.getHeight());
        g.fillRect(0,0,jPanel1.getWidth(),jPanel1.getHeight());
        
        for (contX=0;contX<DIMX;contX++){
            for (contY=0;contY<DIMY;contY++){
                //g.fillOval((int)(pesos[contX][contY][0] * DESENHO_MULT), (int)(pesos[contX][contY][1] * DESENHO_MULT),3,3);
                
                //desenha a linha horizontal
                			
                g.setColor(Color.green);
                if (contX<DIMX-1){
                    g.drawLine((int)(pesos[contX][contY][0] * DESENHO_MULT), (int)(pesos[contX][contY][1] * DESENHO_MULT),(int)(pesos[contX+1][contY][0] * DESENHO_MULT), (int)(pesos[contX+1][contY][1] * DESENHO_MULT));
                }
                
                //linha vertical
                g.setColor(Color.red);
                if (contY<DIMY-1){
                    g.drawLine((int)(pesos[contX][contY][0] * DESENHO_MULT), (int)(pesos[contX][contY][1] * DESENHO_MULT),(int)(pesos[contX][contY+1][0] * DESENHO_MULT), (int)(pesos[contX][contY+1][1] * DESENHO_MULT));
                }
                
                //linha inclinada para baixo esqurda para direita
                g.setColor(Color.blue);
                if ((contY<DIMY-1) && (contX<DIMX-1)){
                    g.drawLine((int)(pesos[contX][contY][0] * DESENHO_MULT), (int)(pesos[contX][contY][1] * DESENHO_MULT),(int)(pesos[contX+1][contY+1][0] * DESENHO_MULT), (int)(pesos[contX+1][contY+1][1] * DESENHO_MULT));
                }

                //linha inclinada para baixo direita para esqurda 
                g.setColor(Color.orange);
                if ((contY<DIMY-1) && (contX>0)){
                    g.drawLine((int)(pesos[contX][contY][0] * DESENHO_MULT), (int)(pesos[contX][contY][1] * DESENHO_MULT),(int)(pesos[contX-1][contY+1][0] * DESENHO_MULT), (int)(pesos[contX-1][contY+1][1] * DESENHO_MULT));
                }

            }
        }
    }
    
    
    public void inicializaPesos(){
        
        //sorteia aleatoriamente os pesos
        
        pesos = new double[DIMX][DIMY][DIMZ];
        Random rnd = new Random();
        int xPeso;
        int yPeso;
        int zPeso;
        
        System.out.println("Inicializando Pesos...");
        
        for (xPeso = 0; xPeso < DIMX; xPeso++){
            for (yPeso = 0; yPeso < DIMY; yPeso++){
                for (zPeso = 0; zPeso < DIMZ; zPeso++){
                    rnd.setSeed(rnd.nextInt());
                    pesos[xPeso][yPeso][zPeso] = 1 - (rnd.nextDouble());
                    
                    pesos[xPeso][yPeso][zPeso] = (pesos[xPeso][yPeso][zPeso]) * 10;
                    
                    
                    //imprime os pesos sorteados
                    System.out.print("X:" + xPeso + " Y:" + yPeso + " Z:" + zPeso + " VALOR: " );
                    System.out.println(pesos[xPeso][yPeso][zPeso]);
                }
            }
        }
        
        //ao sortear os pesos deve pintar a malha        
        desenha();
    }
    
    public double calculaDistancia(int xEntrada, int yEntrada, int x, int y){
        //calcula a distancia euclidiana entre os pontos
        return (Math.pow(xEntrada - pesos[x][y][0], 2) + Math.pow(yEntrada - pesos[x][y][1], 2));
    }
    
    public void atualizaPesos(double xEntrada, double yEntrada, int x, int y, double raioVizinhanca){
        double ca;//coeficiente de aprendizado

        int limX = DIMX;
        int limY = DIMY;
        int iniX = 0;
        int iniY = 0;
        
        int cntX;
        int cntY;
        
        try{
            //pega o grau de vizinhanca especificado
            grauVizinhanca = Integer.valueOf(jTextField4.getText()).intValue();
        }
        catch(NumberFormatException e){
            //se o formato for invalido
            jTextField4.setText("1");
            grauVizinhanca = 1;
        }
                
        limX = x + grauVizinhanca;
        if (limX > DIMX){
        	limX = DIMX;
        }
        
        limY = y + grauVizinhanca;
        if (limY > DIMY){
        	limY = DIMY;
        }


        iniX = x - grauVizinhanca;
        if (iniX < 0){
        	iniX = 0;
        }

        iniY = y - grauVizinhanca;
        if (iniY < 0){
        	iniY = 0;
        }
        
        
        //calcula os pesos dos vizinhos conforme grau de profundidade especificado na interface
        for (cntY=iniY;cntY<limY;cntY++){
	        for (cntX=iniX;cntX<limX;cntX++){
                        //caso for 2.178 usa gauss
		        if (alpha == 2.178){
		            ca = Math.pow(2.178,-((Math.pow(yEntrada - y, 2) 
		                                   + Math.pow(xEntrada - x, 2)) 
		                                        / (2 * Math.pow(raioVizinhanca,2))));
		        } else {
		            ca = alpha;
		        }
		        
		        ca = ca / 10;
		        
		        //System.out.println(ca);
		        
                //efetua o calculo
		        pesos[cntX][cntY][0] = (pesos[cntX][cntY][0] + ca * (xEntrada - pesos[cntX][cntY][0]));
		        pesos[cntX][cntY][1] = (pesos[cntX][cntY][1] + ca * (yEntrada - pesos[cntX][cntY][1]));
		    }
		}
    }

    public int vizinho(Kohonen.Ponto p, int entradaX, int entradaY){
        //procura pelo vizinho da entrada entradaX entradaY

        int iCountX=0;
        int iCountY=0;
        
        int limX = DIMX;
        int limY = DIMY;
        int iniX = 0;
        int iniY = 0;
        
        int posX = p.x;
        int posY = p.y;
        
        double distVizinho=0;
        double distAux=0;
        
        //coordenadas dos vizinhos;
        int vX=0;
        int vY=0;
        
        iniX=0;
        iniY=0;
        limX=DIMX;
        limY=DIMY;
              
        
        //varre a matriz ate encontrar o mais proximo da entrada
        for (iCountX = iniX; iCountX < limX ; iCountX++){
            for (iCountY = iniY; iCountY < limY; iCountY++){
                if (iCountX!=posX && iCountY!=posY){
					distAux = calculaDistancia( entradaX, entradaY, iCountX, iCountY);
                    if(distVizinho==0 || distVizinho > distAux ){
                        vX = iCountX;
                        vY = iCountY;
                        distVizinho = distAux;
                    }
                }
            }
        }
        
        p.x = vX;
        p.y = vY;
        p.raio = distVizinho;
        
        return 0;
    }
    
    public void calcula(int figura){
        //interacao da rede neural
        
        int contFig;//contador das entradas a serem fornecidas para a rede neural
        int interacao;//contador de interacao
        
        int numInterac = Integer.valueOf(jTextField1.getText()).intValue();//le o numero de interacaoes a serem realizadas
        int numRefresh = Integer.valueOf(jTextField2.getText()).intValue();//le a taxa de atualizacao do desenho

        Kohonen.Ponto p = new Kohonen.Ponto();
        
        Graphics g = jPanel1.getGraphics();
        //pega coeficiente de aprendizado da interface
        alpha = Double.valueOf(jTextField3.getText()).doubleValue();
                        
        DESENHO_MULT = jPanel1.getWidth()/10;
        
        for (interacao=0;interacao<numInterac;interacao++){

            if ((((interacao % numRefresh) == 0) || (interacao==numInterac-1))){
                g.clearRect(0,0,jPanel1.getWidth(),jPanel1.getHeight());
            }
            
            for (contFig=0;contFig<18;contFig++){
                //procura pelo vizinho
                vizinho(p, figuras[contFig][0][figura] ,figuras[contFig][1][figura]);//vizinho do circulo 
                //atualiza a vizinhanca pelo grau
                atualizaPesos(figuras[contFig][0][figura], figuras[contFig][1][figura], p.x, p.y, p.raio);
                
                if ((interacao % numRefresh == 0) && (contFig==18-1)){
                    //desenha a malha
                    desenha();
                    try{
                        //chama um delay
                    } 
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
                
            }            
            
        }           
        
        desenha();
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jPanel1.setBorder(new javax.swing.border.MatteBorder(new java.awt.Insets(1, 1, 1, 1), new java.awt.Color(0, 0, 204)));
        jPanel1.setAlignmentX(1.0F);
        jPanel1.setAlignmentY(1.0F);
        jPanel1.setMinimumSize(new java.awt.Dimension(500, 500));
        jPanel1.setPreferredSize(new java.awt.Dimension(500, 500));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        gridBagConstraints.weightx = 13.0;
        gridBagConstraints.weighty = 5.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        jButton1.setMnemonic('c');
        jButton1.setText("Calcular Pesos");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton1MouseReleased(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        getContentPane().add(jButton1, gridBagConstraints);

        jTextField1.setText("300");
        jTextField1.setName("jtfInteracoes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jTextField1, gridBagConstraints);

        jTextField2.setText("50");
        jTextField2.setName("jtfRefresh");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 2;
        getContentPane().add(jTextField2, gridBagConstraints);

        jLabel1.setText("Interacoes: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(jLabel1, gridBagConstraints);

        jLabel2.setText("Refresh:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(jLabel2, gridBagConstraints);

        jLabel3.setText("Modelo Kohonen");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 4;
        getContentPane().add(jLabel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel2, gridBagConstraints);

        jLabel4.setText("Figura:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel4, gridBagConstraints);

        jLabel5.setText("Coef. de aprend:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel5, gridBagConstraints);

        jTextField3.setText("2.178");
	    jTextField3.setToolTipText("Lower values here give more precision");
	    
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 62;
        getContentPane().add(jTextField3, gridBagConstraints);

        jButton2.setMnemonic('r');
        jButton2.setText("Reiniciar...");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                m(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
        getContentPane().add(jButton2, gridBagConstraints);

        jLabel7.setText("Grau Vizinhanca:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jLabel7, gridBagConstraints);

        jTextField4.setText("4");
        jTextField3.setToolTipText("Higher values here ajust the form faster use at max 7");
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 48;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(jTextField4, gridBagConstraints);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Quadrado", "Circulo", "Triangulo", "Traprezio", "Hexagono" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jComboBox1, gridBagConstraints);

        jButton4.setMnemonic('g');
        jButton4.setText("Gauss");
        jButton4.setToolTipText("Gauss");
        jButton4.setMinimumSize(new java.awt.Dimension(49, 25));
        jButton4.setPreferredSize(new java.awt.Dimension(49, 25));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton4MouseReleased(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(jButton4, gridBagConstraints);

        jButton3.setText("About");
        jButton3.setMinimumSize(new java.awt.Dimension(100, 25));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        getContentPane().add(jButton3, new java.awt.GridBagConstraints());

        pack();
    }//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // Add your handling code here:
        new javax.swing.JOptionPane().showMessageDialog(null,"Autor: Marcelo Ivan Martin\nProfessora: Rejane Frozza\nInteligencia Artificial","Kohonen",JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // Add your handling code here:
        jTextField3.setText("2.178");
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseReleased
        // Add your handling code here:
        jTextField3.setText("2.178");
    }//GEN-LAST:event_jButton4MouseReleased

    private void m(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_m
        // Add your handling code here:
        inicializaPesos();
    }//GEN-LAST:event_m

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Add your handling code here:
        calcula(jComboBox1.getSelectedIndex());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseReleased
        // Add your handling code here:
        calcula(jComboBox1.getSelectedIndex());
    }//GEN-LAST:event_jButton1MouseReleased
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new Kohonen().show();
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
    
    private class Ponto{
        public int x=0;
        public int y=0;
        public int z=0;
        public double raio=0;
    }
    
}
