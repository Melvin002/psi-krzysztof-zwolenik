import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Perceptron {

    double[] wagi;
    double progAktywacji;

    public Perceptron() {
    	
    	wagi = new double[2];
    	Random r = new Random();
        // INICJALIZACJA WAG
        for (int i = 0; i < 2; i++) {
            wagi[i] = r.nextDouble();
            System.out.println(wagi[i]);
        }
    }
    public void Train(double[][] wejscie, int[] wyjsie, double aktywacja, double wspUczenia) {
        this.progAktywacji = aktywacja;
        int n = wejscie[0].length;//2
        int p = wyjsie.length;//4
        wagi = new double[n];
        

        for (int i = 0; i < 10000; i++) {
            int bladCalkowity = 0;
            for (int j = 0; j < p; j++) {
                int output = FunkcjaAktywacji(wejscie[j]);
                int blad = wyjsie[j] - output;
                bladCalkowity += Math.abs(blad);
                for (int k = 0; k < n; k++) {
                    double delta = wspUczenia * wejscie[j][k] * blad;
                    wagi[k] += delta;
                }
            }
            if (bladCalkowity == 0) break;
        }

    }

    public int FunkcjaAktywacji(double[] wejscie) {
        double suma = 0.0;
        suma = wagi[0] * wejscie[0] + wagi[1] * wejscie[1];
        System.out.println(suma);
        if (suma >= progAktywacji) return 1;
        else return -1;
    }

    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        Perceptron p1 = new Perceptron();
        Perceptron p2 = new Perceptron();
        Perceptron p3 = new Perceptron();
        //Dane dla koniunkcji
        double wejscie [][] = {{ 15,0},//NIEBIESKIE
        						{23,0},
        						{15,7},
        						{23,7},
        						{17,3},
        						{19,5},
        						{18,0},
        						{22,1},
        						{15,4},
        						{9,14},//CZERWONE
        						{9,20},
        						{13,14},
        						{13,20},
        						{9,15},
        						{10,17},
        						{11,16},
        						{12,19},
        						{9, 14},
        						{0,0},//ZIELONE
        						{0,8},
        						{8,0},
        						{8,8},
        						{2,3},
        						{2,7},
        						{4,1},
        						{5,3},
        						{7,0}};
       int wyjscieNieb[] = {1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
       int wyjscieCzerw[] = {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0};
       int wyjscieZiel[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1};
       p1.Train(wejscie,wyjscieNieb,0,0.01);
       p2.Train(wejscie,wyjscieCzerw,0,0.01);
       p3.Train(wejscie,wyjscieZiel,0,0.001);

       int x,y;
       while(true){
    	   System.out.println("Podaj wartosc x i y");
    	   x = in.nextInt();
    	   y = in.nextInt();
    	   if(x == 666)
    		   break;
    	   if(p1.FunkcjaAktywacji(new double[]{x,y}) == 1)
    		   System.out.println("Niebieskie");
    	   if(p2.FunkcjaAktywacji(new double[]{x,y}) == 1)
    		   System.out.println("Czerwone");
    	   if(p3.FunkcjaAktywacji(new double[]{x,y}) == 1)
    		   System.out.println("Zielone");
    	   
       }

    }
}
/*
Zbi�r danych
NIEBIESKIE
y 0,7
x 15-23
	15,0
	23,0
	15,7
	23,7
	17,3
	19,5
	18,0
	22,1
	15,4
CZERWONE
y 14-20
x 9-13
	6,14
	6,20
	13,14
	13,20
	8,15
	9,17
	11,16
	12,19
	7, 14
ZIELONE
y 0-7
x 0-7
	0,0
	0,9
	9,0
	9,9
	2,3
	2,8
	4,1
	5,3
	7,0
*/